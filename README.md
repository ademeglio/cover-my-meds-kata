# About  
Initial project file for CoverMyMeds Kata, 10/16/2020 11:30 AM with Leah.

## Live Coding Interview Details from Megan Murray
>Hi Anthony,
>  
>Thank you for sending your availability! We have your live coding interview scheduled for Friday, October 16th at 
>11:30 AM EST.
>  
>**What you can expect:**
>  
>Leah, one of our Software Developers, will join you for a video and screen share meeting. They 
>will send you 3 different coding exercises to walk through different areas of the stack. The 
>first coding exercise will be client-side (CSS, HTML, JavaScript). The second will be backend, 
>using any text-parsing language you’d like (Ruby, Python, PHP, C#, etc.). The third will be a 
>few SQL operations (setting up schemas, populating tables, selects, joins, etc.). They will 
>spend 30 minutes live on each exercise with you. For any work you two decide should be finished 
>up after that 30 minutes, you’ll have the opportunity to complete it on your own time and send 
>it back to them.
>  
>**What we look for:**
>  
>We’d like you to approach this meeting as if you’re mentoring our Software Developer. We 
>encourage you to explain your approach, be collaborative and ask questions. The goal of taking 
>you through different parts of the stack is to confirm your areas of strength and passion. We 
>also like to see fluency with your chosen tools (see below). It’s totally expected for you to 
>use search engines while you’re problem-solving, we just ask that you’re mindful of how much 
>you’re relying on them.
>  
>**Tools to use:**
>  
>You'll be working through the exercises on your own machine, using the tools of your choice. I 
>recommend making sure you have these set up and ready to go ahead of time.