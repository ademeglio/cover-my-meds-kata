package com.covermymeds;

public class Line {

    private String line;
    private int maxLineCharacters;

    public Line(String line) {
        this.line = line;
        this.maxLineCharacters = 80;
    }

    public String getLine() {
        return line;
    }

    @Override
    public String toString() {
        return line;
    }
}
