package com.covermymeds;

import java.util.regex.Pattern;

public class Word {

    private String word;
    private int charCount;

    // Constructor
    public Word(String word) {

        if (word.contains("\n")) {
            word = word.replaceAll("^[(\n)]", " ");
            word = word.replaceAll("^\\s+", "");
            word = word.replaceFirst("\\b(\\n)\\b", " ");
        }
        if (word.endsWith("\n\n")) {
            word = word.replaceFirst( "(\n\n)", "\n");
            this.word = word;
        }else {
            this.word = word + ' ';
        }
        charCount = this.word.length();
    }


    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getCharCount() {
        return charCount;
    }

    @Override
    public String toString() {
        return word;
    }

}
