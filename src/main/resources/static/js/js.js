const friendsBlock = document.getElementById("friends_list");
var friends = [];

populateFriends();


function populateFriends() {
    createFriend(
        "Hello",
        "Ray",
        "#d30505",
        "#d8fbce",
        "'Courier New', Courier, monospace",
        "1em")
    createFriend(
        "Yo!",
        "Oscar",
        "orange",
        "blue",
        "Impact, Charcoal, sans-serif",
        "2em")
    createFriend(
        "Bueno",
        "Gwen",
        "#3e661a",
        "#fba6ac",
        "Arial, Helvetica, sans-serif",
        "1.5em")
    createFriend(
        "Howz it",
        "Bob",
        "blue",
        "yellow",
        "'Trebuchet MS', Helvetica, sans-serif",
        "1.25em")
    createFriend(
        "Hola!",
        "Marie",
        "#dd04dd",
        "cyan",
        "'Comic Sans MS', cursive, sans-serif",
        "2em")

    randomizeFriends(friends);
}

function createFriend(greeting,
                      friendName,
                      friendFavColor,
                      contrastingColor,
                      fontFamily,
                      fontEmSize) {

    var friendDiv = document.createElement('div');
    friendDiv.id = "div" + friendName;

    var friendP = document.createElement('p');
    friendP.innerHTML = greeting + ', ' + friendName;
    friendP.style.color = friendFavColor;
    friendP.style.backgroundColor = contrastingColor;
    friendP.style.fontFamily = fontFamily;
    friendP.style.fontSize = fontEmSize;

    friendDiv.appendChild(friendP);
    friends.push(friendDiv)
}



function randomizeFriends(friendsArray) {
    // Did some research on the best way to randomize and discovered
    // the Fisher-Yates Algorithm

    for (let i = friendsArray.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * i);
        const temp = friendsArray[i];
        friendsArray[i] = friendsArray[j];
        friendsArray[j] = temp
        friendsBlock.appendChild(friendsArray[i])
        friendsBlock.appendChild(friendsArray[j]);
    }
}

// friendsBlock.appendChild(friends[i]);